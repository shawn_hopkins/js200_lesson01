
// create a new row in the relation table
function add_relations(){

    let count = document.getElementById('count');
    let id = parseInt( count.innerHTML);
    let tbl = document.getElementById('relations').getElementsByTagName('tbody')[0];


    let row  = tbl.insertRow(tbl.rows.length);

    let cell0=row.insertCell(0);

    let t0=document.createElement("select");
    t0.setAttribute('id','prel'+id);
    let options=['mother','father','sister','brother','uncle','aunt'];
    for(i=0;i<options.length;i++){
        let option=document.createElement('option');
        option.text=options[i];
        t0.appendChild(option);
    }

    cell0.appendChild(t0);

    let cell1=row.insertCell(1);
    let t1=document.createElement("input");
    t1.type='text';
    t1.id = "pfnam"+id;
    cell1.appendChild(t1);

    let cell2=row.insertCell(2);
    let t2=document.createElement("input");
    t2.id = "plnam"+id;
    t2.type='text';
    cell2.appendChild(t2);

    let cell3=row.insertCell(3);
    let t3=document.createElement("input");
    t3.id = "pcode"+id;
    t3.type='number';
    cell3.appendChild(t3);

    count.innerHTML=(id +1).toString();
}
// create user constructor
function User(relation,fnam,lnam,yearsCoding){
    this.relation=relation;
    this.fnam = fnam;
    this.lnam=lnam;
    this.yearsCoding=yearsCoding;
}
// generate summary of years codign
function create_report(){

    let data = create_report_data();
    let html = document.getElementById('report_summary');
    let numRels = data.relations.length;

    let totYears=0;
    let myYears=parseInt(data.yearsCoding);

    for(i=0;i<data.relations.length;i++){
        totYears += parseInt(data.relations[i].yearsCoding);
    }

    let diff=Math.abs(totYears-myYears);

    let raw;

    if(myYears!=totYears) {
        raw = `${data.fnam} ${data.lnam} has ${diff}  ${myYears > totYears ? "more" : "less"} years programming experience than all ${numRels} relatives put together.`;
    }
    else{
        raw=`${data.lnam}, has ${data.fnam} and his collective family have been programming for ${totYears} years.`;
    }

    html.innerHTML=raw;

}
// read the data from teh years coding table
function create_report_data(){

    let myself = new User('self',document.getElementById('fname').value,
        document.getElementById('lname').value,
        document.getElementById('coding').value);

    let tbl = document.getElementById('relations');

    let relations=[];

    for(i=1;i<tbl.rows.length;i++){

        let rel = new User();

        let cells = tbl.rows.item(i).cells;

        rel.relation =tbl.rows[i].cells[0].childNodes[0].value;
        rel.fnam =  tbl.rows[i].cells[1].childNodes[0].value;
        rel.lnam = tbl.rows[i].cells[2].childNodes[0].value;
        rel.yearsCoding =tbl.rows[i].cells[3].childNodes[0].value;

        relations.push(rel);
    }

    myself.relations=relations;

    return myself;
}

