/*Using the Math object, put together a code snippet
that allows you to draw a random card with a value
between 1 and 13 (assume ace is 1, jack is 11…).*/

const cards = ['Ace', 'Two', 'Three', 'Four', 'Five', 'Six', 'Seven', 'Eight', 'Nine', 'Ten', 'Jack', 'Queen', 'King'];

// Math Ex 1
function draw_card(){

    var val = Math.floor(Math.random() * 13);
    img = document.getElementById('hs1');
    img.src = 'Static\\' + cards[val] + ".png"
    document.getElementById('hs-num').innerHTML=val;
}

// Math Ex 2
function draw_cards(nCards){

    var cardindex=[] , raw;
    var html = document.getElementById('ex2');
    html.innerHTML="";


    for(i=1;i<=nCards;i++) {

        // Generate random number with no reselection
        var val = Math.floor(Math.random() * 13);
        while(cardindex.indexOf(val)>-1){
            val = Math.floor(Math.random() * 13);
        }
        cardindex.push(val);

        raw =     `<div class='col' id='d${i}'>` +
                  `<img id='p${i}' width='30' height='30'  src='Static\\pointdown.png' style='visibility:hidden;'><br /> ` +
                  `<img id='c${i}'  width='130' height='250' src = 'Static\\${cards[val]}.png' ><br />` +
                  `<img id='b${i}' width='30' height='30'  src='Static\\point.png' style='visibility: hidden;'> ` +
                  "</div>";

        html.innerHTML += raw;
    }
    // Identify the biggest card
    var maxval = (Math.max(...cardindex));
    var maxidx = cardindex.indexOf(maxval) + 1;
    document.getElementById('p' + maxidx.toString()).style.visibility='visible';
    document.getElementById('b' + maxidx.toString()).style.visibility='visible';
}


// Math Ex 3 & 4
function pizza_calc(dia,id){

    // least square linear regression model based on pizza data provided
    var cost = parseFloat( (0.75 * dia) + 7.24).toFixed(2);
    var cost_sq =  parseFloat( parseFloat(cost) / (Math.PI * (dia/2)**2)).toFixed(2);

    document.getElementById('wid' + id ).innerHTML=dia;
    document.getElementById('cost' + id).innerHTML=cost;
    document.getElementById( 'sqin' + id).innerHTML=cost_sq;

}




