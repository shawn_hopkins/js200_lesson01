function get_middle(str,end){

    var d1 = get_date(str);
    var d2 = get_date(end);

    var diff = new Date ((d2.getTime() + d1.getTime())/2);


    var mid = document.getElementById('middle');

    mid.value=format(diff,'yyyy-MM-dd');

}

function get_date(iso){
    var y,m,d;
    var isodt=iso.split('-');
    y=isodt[0];
    m=isodt[1]-1;
    d=isodt[2];
    return new Date(y,m,d);
}

format = function date2str(x, y) {
    var z = {
        M: x.getMonth() + 1,
        d: x.getDate(),
        h: x.getHours(),
        m: x.getMinutes(),
        s: x.getSeconds()
    };
    y = y.replace(/(M+|d+|h+|m+|s+)/g, function(v) {
        return ((v.length > 1 ? "0" : "") + eval('z.' + v.slice(-1))).slice(-2)
    });

    return y.replace(/(y+)/g, function(v) {
        return x.getFullYear().toString().slice(-v.length)
    });
}