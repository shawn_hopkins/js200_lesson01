
// Create the X and O mark in alternating turns
function  draw_mark(id) {
    let cell = document.getElementById(id);
    let mark = cell.childNodes[0];
    let turn = document.getElementById('turn');
    
    let current=mark.getAttribute('class');
    if (current==null){

        let marker = (parseInt(turn.innerHTML) % 2 ? 'fa-times' : 'fa-circle-o' )
        let markerStyle = (parseInt(turn.innerHTML) % 2 ? 'red' : 'blue' )
        mark.setAttribute('class',`fa ${marker}`);
        mark.style.color=markerStyle;

        turn.innerHTML=parseInt(turn.innerHTML) + 1;
    }
}

// Draw the board
function draw_table(){
    let html = document.getElementById('tbl');

    let mda=[[0,1,2],[3,4,5],[6,7,8]];
    for(i=0;i<mda.length;i++) {

        let row  = tbl.insertRow(tbl.rows[i]);

        for (j = 0; j < mda[i].length; j++)
        {
            let cell0=row.insertCell(j);
            cell0.setAttribute('id',`td_r${i}c${j}`);
            cell0.setAttribute('onclick','draw_mark(this.id)');

            let field=document.createElement("i");
            field.setAttribute('id',`r${i}c${j}`);
            cell0.appendChild(field);
        }
    }
}

// Write the values from the board into html
function draw_array() {
    let data = convertToArray();

    let html=document.getElementById('code');
    html.innerHTML="";

    let  raw ='<div class="col">';

    for(i=0;i<data.length;i++){
        for(j=0;j<data[i].length;j++){

            raw += `<span style="color:blue;font-size: xx-large;margin:10px;">${data[i][j]}</span>`;
        }
        raw += '<br />';
    }

    html.innerHTML += raw + '</div>';
}

// read the values from the board into an array
function convertToArray(){

    let tbl = document.getElementById('tbl');

    let result=[];

    for(i=0;i<tbl.rows.length;i++){

        let rowarr=[];
        let cells = tbl.rows.item(i).cells;

        for(j=0;j<3;j++) {
            let id = (cells[j].getAttribute('id')).slice(3);

            let  mark = document.getElementById(id).getAttribute('class');

            if (mark != null) {
                rowarr.push( (mark.indexOf('times')>0 ? 'X' : "O"));
            } else {
                rowarr.push("-");
            }
        }
        result.push(rowarr);
    }
    return result;

}